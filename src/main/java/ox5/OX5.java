package ox5;

import javafx.application.Application;
import javafx.stage.Stage;
import ox5.controls.GameController;

public class OX5 extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        new GameController(stage);
    }
}
