package ox5.controls;

import ox5.enums.Difficulty;

public interface DifficultyController {
    void setDifficulty(Difficulty difficulty);
}
