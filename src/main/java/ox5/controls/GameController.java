package ox5.controls;

import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import ox5.controls.buttons.ButtonArray;
import ox5.controls.buttons.DifficultyButtonArray;
import ox5.controls.buttons.ModeButtonArray;
import ox5.enums.Difficulty;
import ox5.enums.Mode;
import ox5.game.AIGame;
import ox5.game.Game;

import static ox5.game.elements.symbols.StandardSymbol.CIRCLE;
import static ox5.game.elements.symbols.StandardSymbol.CROSS;

public final class GameController implements ModeController, DifficultyController {

    private final ButtonArray<Difficulty> difficultyButtons = new DifficultyButtonArray(this);
    private final VBox menuBox = new VBox();
    private final HBox rootBox = new HBox();

    private Game game;
    private Difficulty difficulty;

    public GameController(Stage stage) {

        ButtonArray<Mode> modeButtons = new ModeButtonArray(this);
        modeButtons.setValue(Mode.TWO_PLAYERS);
        difficultyButtons.setValue(Difficulty.MEDIUM);
        difficultyButtons.disable();

        menuBox.setStyle("-fx-background-color: #eff0f1");
        menuBox.getChildren().addAll(modeButtons.getBoxes());
        menuBox.getChildren().addAll(difficultyButtons.getBoxes());

        stage.setResizable(false);
        stage.getIcons().add(new Image("/resources/icon.png"));
        stage.setScene(new Scene(rootBox, 740, 640));
        stage.sizeToScene();
        stage.show();
    }

    @Override
    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
        game.setDifficulty(difficulty);
    }

    @Override
    public void setMode(Mode mode) {
        if (mode == Mode.SINGLE_PLAYER_CIRCLE) {
            game = new AIGame(CIRCLE, difficulty);
            difficultyButtons.enable();
        } else if (mode == Mode.SINGLE_PLAYER_CROSS) {
            game = new AIGame(CROSS, difficulty);
            difficultyButtons.enable();
        } else {
            game = new Game();
            difficultyButtons.disable();
        }
        rootBox.getChildren().clear();
        rootBox.getChildren().addAll(game.getGameBox(), menuBox);
    }
}
