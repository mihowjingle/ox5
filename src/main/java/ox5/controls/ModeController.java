package ox5.controls;

import ox5.enums.Mode;

public interface ModeController {
    void setMode(Mode mode);
}
