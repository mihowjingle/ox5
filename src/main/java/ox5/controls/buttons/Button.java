package ox5.controls.buttons;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import lombok.Getter;

import java.util.EnumMap;
import java.util.Map;

class Button {

    @Getter
    private final ImageView box;
    private final Map<ButtonState, Image> stateImages = new EnumMap<>(ButtonState.class);

    @Getter
    private ButtonState state = ButtonState.BASE;

    public void setState(ButtonState state) {
        this.state = state;
        this.box.setImage(stateImages.get(state));
    }

    Button(String baseImgUrl, String mouseoverImgUrl, String activeImgUrl) {

        stateImages.put(ButtonState.BASE, new Image(baseImgUrl));
        stateImages.put(ButtonState.MOUSEOVER, new Image(mouseoverImgUrl));
        stateImages.put(ButtonState.ACTIVE, new Image(activeImgUrl));

        this.box = new ImageView(stateImages.get(state));
    }
}
