package ox5.controls.buttons;

import javafx.scene.image.ImageView;
import lombok.Getter;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

public abstract class ButtonArray<TYPE extends Enum<TYPE>> {

    final Map<TYPE, Button> buttons;

    @Getter
    private boolean enabled;

    TYPE value;

    ButtonArray(Class<TYPE> type) {
        buttons = new EnumMap<>(type);
        enabled = true;
    }

    public List<ImageView> getBoxes() {
        return buttons.values().stream().map(Button::getBox).collect(toList());
    }

    public void setValue(TYPE value) {
        this.value = value;
        if (enabled) buttons.forEach((type, button) -> {
            if (type == value) button.setState(ButtonState.ACTIVE);
            else button.setState(ButtonState.BASE);
        });
    }

    public void enable() {
        enabled = true;
        buttons.forEach((type, button) -> {
            if (type == value) button.setState(ButtonState.ACTIVE);
        });
    }

    public void disable() {
        enabled = false;
        buttons.forEach((type, button) -> button.setState(ButtonState.BASE));
    }
}
