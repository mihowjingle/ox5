package ox5.controls.buttons;

import ox5.controls.DifficultyController;
import ox5.enums.Difficulty;

public class DifficultyButtonArray extends ButtonArray<Difficulty> {

    private final DifficultyController difficultyController;

    public DifficultyButtonArray(DifficultyController difficultyController) {
        super(Difficulty.class);

        this.difficultyController = difficultyController;

        buttons.put(Difficulty.VERY_EASY, new Button(
                "/resources/difficulty/base.png",
                "/resources/difficulty/very_easy/mouseover.png",
                "/resources/difficulty/very_easy/active.png")
        );
        buttons.put(Difficulty.EASY, new Button(
                "/resources/difficulty/base.png",
                "/resources/difficulty/easy/mouseover.png",
                "/resources/difficulty/easy/active.png")
        );
        buttons.put(Difficulty.MEDIUM, new Button(
                "/resources/difficulty/base.png",
                "/resources/difficulty/medium/mouseover.png",
                "/resources/difficulty/medium/active.png")
        );
        buttons.put(Difficulty.HARD, new Button(
                "/resources/difficulty/base.png",
                "/resources/difficulty/hard/mouseover.png",
                "/resources/difficulty/hard/active.png")
        );
        buttons.put(Difficulty.VERY_HARD, new Button(
                "/resources/difficulty/base.png",
                "/resources/difficulty/very_hard/mouseover.png",
                "/resources/difficulty/very_hard/active.png")
        );
        
        buttons.forEach((difficulty, button) -> {

            button.getBox().setOnMouseClicked(event -> {
                if (isEnabled()) {
                    if (value != difficulty)
                        setValue(difficulty);
                }
            });

            button.getBox().setOnMouseEntered(event -> {
                if (isEnabled())
                    if (value != difficulty)
                        button.setState(ButtonState.MOUSEOVER);
            });

            button.getBox().setOnMouseExited(event -> {
                if (isEnabled())
                    if (value != difficulty)
                        button.setState(ButtonState.BASE);
            });
        });
    }

    @Override
    public void setValue(Difficulty value) {
        super.setValue(value);
        difficultyController.setDifficulty(value);
    }
}
