package ox5.controls.buttons;

import ox5.controls.ModeController;
import ox5.enums.Mode;

public class ModeButtonArray extends ButtonArray<Mode> {

    private final ModeController modeController;

    public ModeButtonArray(ModeController modeController) {
        super(Mode.class);

        this.modeController = modeController;

        buttons.put(Mode.TWO_PLAYERS, new Button(
                "/resources/two_players/base.png",
                "/resources/two_players/mouseover.png",
                "/resources/two_players/active.png")
        );
        buttons.put(Mode.SINGLE_PLAYER_CIRCLE, new Button(
                "/resources/single_player_circle/base.png",
                "/resources/single_player_circle/mouseover.png",
                "/resources/single_player_circle/active.png")
        );
        buttons.put(Mode.SINGLE_PLAYER_CROSS, new Button(
                "/resources/single_player_cross/base.png",
                "/resources/single_player_cross/mouseover.png",
                "/resources/single_player_cross/active.png")
        );

        buttons.forEach((mode, button) -> {

            button.getBox().setOnMouseClicked(event -> {
                if (isEnabled()) {
                    setValue(mode);
                }
            });

            button.getBox().setOnMouseEntered(event -> {
                if (isEnabled())
                        button.setState(ButtonState.MOUSEOVER);
            });

            button.getBox().setOnMouseExited(event -> {
                if (isEnabled())
                    if (value == mode)
                        button.setState(ButtonState.ACTIVE);
                    else
                        button.setState(ButtonState.BASE);
            });
        });
    }

    @Override
    public void setValue(Mode value) {
        super.setValue(value);
        modeController.setMode(value);
    }
}
