package ox5.enums;

import java.util.EnumMap;
import java.util.Map;

public enum Difficulty {
    VERY_EASY(96, 92, 88, 6, 3, 3, 2, 0, 0),
    EASY(98, 96, 94, 25, 10, 10, 7, 0, 0),
    MEDIUM(99, 98, 97, 90, 85, 85, 80, 10, 8),
    HARD(100, 100, 100, 95, 95, 95, 95, 55, 50),
    VERY_HARD(100, 100, 100, 100, 100, 100, 100, 100, 100);

    private final Map<SituationType, Integer> chances = new EnumMap<>(SituationType.class);

    Difficulty(int fil, int til, int twg, int dt, int dtom, int tg, int datg, int dtompa, int tri) {

        chances.put(SituationType.FOUR_IN_LINE, fil);
        chances.put(SituationType.THREE_IN_LINE_AI, til);
        chances.put(SituationType.THREE_IN_LINE_HUMAN, til);
        chances.put(SituationType.THREE_WITH_GAP, twg);
        chances.put(SituationType.DOUBLE_TWO, dt);
        chances.put(SituationType.DOUBLE_THREE_OR_MIXED, dtom);
        chances.put(SituationType.THREE_GAPS, tg);
        chances.put(SituationType.DOUBLE_ALMOST_THREE_GAPS, datg);
        chances.put(SituationType.DOUBLE_THREE_PLUS_ANOTHER, dtompa);
        chances.put(SituationType.DOUBLE_MIXED_PLUS_ANOTHER, dtompa);
        chances.put(SituationType.DOUBLE_TWO_PLUS_ANOTHER, dtompa);
        chances.put(SituationType.TRIPLE_THREE, tri);
        chances.put(SituationType.TRIPLE_TWO, tri);
    }

    public Integer getChanceFor(SituationType situationType) {
        return chances.get(situationType);
    }
}
