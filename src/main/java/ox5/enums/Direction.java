package ox5.enums;

import lombok.RequiredArgsConstructor;
import ox5.game.elements.Intersection;

import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;

import static ox5.enums.Direction.Functions.*;

@RequiredArgsConstructor
public enum Direction {

    RIGHT_UP(addition, subtraction, (i1, i2) -> i1.getX() - i2.getX() == i2.getY() - i1.getY()),
    RIGHT(addition, nothing, (i1, i2) -> i1.getY() == i2.getY()),
    RIGHT_DOWN(addition, addition, (i1, i2) -> i1.getX() - i2.getX() == i1.getY() - i2.getY()),
    DOWN(nothing, addition, (i1, i2) -> i1.getX() == i2.getX());

    private final BiFunction<Integer, Integer, Integer> xShift;
    private final BiFunction<Integer, Integer, Integer> yShift;
    private final BiFunction<Intersection, Intersection, Boolean> sameLine;

    static class Functions {
        final static BiFunction<Integer, Integer, Integer> addition = (c, t) -> c + t;
        final static BiFunction<Integer, Integer, Integer> subtraction = (c, t) -> c - t;
        final static BiFunction<Integer, Integer, Integer> nothing = (c, t) -> c;
    }

    public Intersection shift(Intersection startingPoint, int shift) {
        return Intersection.at(xShift.apply(startingPoint.getX(), shift), yShift.apply(startingPoint.getY(), shift));
    }

    public Intersection shift(int x, int y, int shift) {
        return Intersection.at(xShift.apply(x, shift), yShift.apply(y, shift));
    }

    public boolean notTheSameLine(Intersection i1, Intersection i2) {
        return ! sameLine.apply(i1, i2);
    }

    public static Optional<Direction> between(Intersection i1, Intersection i2) {

        for (Direction direction : Direction.values()) {
            if (!i1.equals(i2) && direction.sameLine.apply(i1, i2)) {
                return Optional.of(direction);
            }
        }

        return Optional.empty();
    }

    public static Optional<Direction> between(List<Intersection> intersections) {

        Direction firstDetectedDirection = null;

        if (duplicatesFound(intersections)) return Optional.empty();

        for (int i = 0; i < intersections.size() - 1; i++) {
            boolean anyDirectionFoundThisIteration = false;
            for (Direction direction : Direction.values()) {
                if (direction.sameLine.apply(intersections.get(i), intersections.get(i + 1))) {
                    if (firstDetectedDirection == null) firstDetectedDirection = direction;
                    if (direction != firstDetectedDirection) return Optional.empty();
                    anyDirectionFoundThisIteration = true;
                }
            }
            if (!anyDirectionFoundThisIteration) return Optional.empty();
        }

        return Optional.ofNullable(firstDetectedDirection);
    }

    private static boolean duplicatesFound(List<Intersection> intersections) {

        for (int i = 0; i < intersections.size(); i++)
            for (int j = i + 1; j < intersections.size(); j++)
                if (intersections.get(i).equals(intersections.get(j))) return true;
        return false;
    }

    public int compare(Intersection i1, Intersection i2) {
        final Optional<Direction> detectedDirection = Direction.between(i1, i2);
        if (detectedDirection.isPresent()) {
            final Direction direction = detectedDirection.get();
            if (direction == this) {
                if (direction != Direction.DOWN) {
                    if (i1.getX() > i2.getX()) return 1;
                    if (i1.getX() < i2.getX()) return -1;
                }
                if (direction == Direction.DOWN) {
                    if (i1.getY() > i2.getY()) return 1;
                    if (i1.getY() < i2.getY()) return -1;
                }
            }
        }
        return 0;
    }
}
