package ox5.enums;

public enum Mode {
    TWO_PLAYERS,
    SINGLE_PLAYER_CIRCLE,
    SINGLE_PLAYER_CROSS
}
