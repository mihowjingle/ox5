package ox5.enums;

import lombok.RequiredArgsConstructor;
import lombok.experimental.Delegate;
import ox5.game.ai.detectors.SituationDetector;
import ox5.game.ai.detectors.specific.*;
import ox5.game.ai.random.DefaultRandomizer;

@RequiredArgsConstructor
public enum SituationType {

    DOUBLE_THREE_OR_MIXED(new DoubleThreeOrMixedDetector()),
    DOUBLE_TWO(new DoubleTwoDetector()),
    DOUBLE_ALMOST_THREE_GAPS(new DoubleAlmostThreeGapsDetector()),
    DOUBLE_THREE_PLUS_ANOTHER(new DoubleThreePlusAnotherDetector(DefaultRandomizer.get())),
    DOUBLE_MIXED_PLUS_ANOTHER(new DoubleMixedPlusAnotherDetector()),
    DOUBLE_TWO_PLUS_ANOTHER(new DoubleTwoPlusAnotherDetector(DefaultRandomizer.get())),
    FOUR_IN_LINE(new FourInLineDetector()),
    THREE_IN_LINE_AI(new ThreeAIDetector()),
    THREE_IN_LINE_HUMAN(new ThreeHumanDetector(DefaultRandomizer.get())),
    THREE_WITH_GAP(new ThreeWithGapDetector()),
    THREE_GAPS(new ThreeGapsDetector()),
    TRIPLE_THREE(new TripleThreeDetector(DefaultRandomizer.get())),
    TRIPLE_TWO(new TripleTwoDetector(DefaultRandomizer.get()));

    @Delegate
    private final SituationDetector detector;
}
