package ox5.enums;

import ox5.game.elements.symbols.StandardSymbol;

import static ox5.game.elements.symbols.StandardSymbol.*;

public enum State {

    CROSS_WIN,
    CIRCLE_WIN,
    DRAW,
    RUNNING;

    /**
     * @param symbol for which to return victory
     * @return state indicating victory of given symbol
     */
    public static State win(StandardSymbol symbol) {
        return symbol == CIRCLE ? CIRCLE_WIN : CROSS_WIN;
    }

    public boolean isWin() {
        return this == CIRCLE_WIN || this == CROSS_WIN;
    }
}
