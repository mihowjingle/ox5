package ox5.game;

import ox5.enums.Difficulty;
import ox5.enums.Mode;
import ox5.enums.State;
import ox5.game.ai.AIPlayer;
import ox5.game.ai.random.DefaultRandomizer;
import ox5.game.ai.random.Randomizer;
import ox5.game.elements.Board;
import ox5.game.elements.Intersection;
import ox5.game.elements.symbols.StandardSymbol;

import java.util.concurrent.atomic.AtomicInteger;

import static ox5.game.elements.symbols.EmptySymbol.EMPTY;
import static ox5.game.elements.symbols.StandardSymbol.CIRCLE;

public class AIGame extends Game {

    private final StandardSymbol humanSymbol;
    private final AIPlayer aiPlayer;

    public AIGame(StandardSymbol humanSymbol, Difficulty difficulty) {
        Randomizer randomizer = DefaultRandomizer.get();
        this.aiPlayer = new AIPlayer(board, randomizer, humanSymbol.other(), difficulty, this);
        this.humanSymbol = humanSymbol;
        if (this.humanSymbol == CIRCLE) {
            this.mode = Mode.SINGLE_PLAYER_CIRCLE;
        } else {
            this.mode = Mode.SINGLE_PLAYER_CROSS;
            this.aiPlayer.play();
            update();
        }

        for (AtomicInteger ax = new AtomicInteger(0); ax.get() < Board.SIZE; ax.incrementAndGet()) {
            for (AtomicInteger ay = new AtomicInteger(0); ay.get() < Board.SIZE; ay.incrementAndGet()) {
                final int x = ax.get();
                final int y = ay.get();
                boardView[x][y].setOnMouseClicked(click -> {
                    if (state == State.RUNNING) {
                        Intersection whereClicked = Intersection.at(x, y);
                        if (currentSymbol == this.humanSymbol && insertSymbol(currentSymbol, whereClicked)) {
                            update();
                            if (state == State.RUNNING) aiPlayer.play();
                            update();
                        }
                    }
                });
                boardView[x][y].setOnMouseEntered(enter -> {
                    if (state == State.RUNNING) {
                        if (board.getSymbolAt(x, y) == EMPTY && currentSymbol == this.humanSymbol) {
                            if (currentSymbol == CIRCLE) boardView[x][y].setImage(blurredCircle);
                            else boardView[x][y].setImage(blurredCross);
                        }
                    }
                });
            }
        }
    }

    @Override
    public void setDifficulty(Difficulty difficulty) {
        aiPlayer.setDifficulty(difficulty);
    }
}