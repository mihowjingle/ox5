package ox5.game;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import lombok.Getter;
import ox5.enums.Difficulty;
import ox5.enums.Mode;
import ox5.enums.State;
import ox5.game.elements.Board;
import ox5.game.elements.Intersection;
import ox5.game.elements.symbols.StandardSymbol;

import java.util.concurrent.atomic.AtomicInteger;

import static ox5.game.elements.symbols.EmptySymbol.EMPTY;
import static ox5.game.elements.symbols.StandardSymbol.CIRCLE;
import static ox5.game.elements.symbols.WinningSymbol.WINNING_CIRCLE;
import static ox5.game.elements.symbols.WinningSymbol.WINNING_CROSS;

public class Game extends GameLogic {

    private final Image circle = new Image("/resources/board/circle.png");
    private final Image cross = new Image("/resources/board/cross.png");
    private final Image darkCircle = new Image("/resources/board/dark_circle.png");
    private final Image darkCross = new Image("/resources/board/dark_cross.png");
    private final Image empty = new Image("/resources/board/empty.png");
    final Image blurredCircle = new Image("/resources/board/blurred_circle.png");
    final Image blurredCross = new Image("/resources/board/blurred_cross.png");

    ImageView boardView[][];
    Mode mode;

    @Getter
    GridPane gameBox;

    public Game() {
        mode = Mode.TWO_PLAYERS;

        gameBox = new GridPane();
        gameBox.setMaxSize(640, 640);
        gameBox.setMinSize(640, 640);
        boardView = new ImageView[Board.SIZE][Board.SIZE];

        for (AtomicInteger ax = new AtomicInteger(0); ax.get() < Board.SIZE; ax.incrementAndGet()) {
            for (AtomicInteger ay = new AtomicInteger(0); ay.get() < Board.SIZE; ay.incrementAndGet()) {
                final int x = ax.get();
                final int y = ay.get();
                boardView[x][y] = new ImageView(empty);
                gameBox.add(boardView[x][y], x, y);

                boardView[x][y].setOnMouseClicked(click -> {
                    if (state == State.RUNNING) {
                        Intersection whereClicked = Intersection.at(x, y);
                        if (insertSymbol(currentSymbol, whereClicked)) update();
                    }
                });
                boardView[x][y].setOnMouseEntered(enter -> {
                    if (state == State.RUNNING) {
                        if (board.getSymbolAt(x, y) == EMPTY) {
                            if (currentSymbol == CIRCLE) boardView[x][y].setImage(blurredCircle);
                            else boardView[x][y].setImage(blurredCross);
                        }
                    }
                });
                boardView[x][y].setOnMouseExited(exit -> {
                    if (state == State.RUNNING) {
                        if (board.getSymbolAt(x, y) == EMPTY) {
                            boardView[x][y].setImage(empty);
                        }
                    }
                });
            }
        }
    }

    @Override
    void update() {
        super.update();
        if (state.isWin()) {
            for (int x = 0; x < Board.SIZE; x++)
                for (int y = 0; y < Board.SIZE; y++) {
                    if (board.getSymbolAt(x, y) == WINNING_CIRCLE) {
                        gameBox.add(new ImageView(darkCircle), x, y);
                    } else if (board.getSymbolAt(x, y) == WINNING_CROSS) {
                        gameBox.add(new ImageView(darkCross), x, y);
                    }
                }
        }
    }

    @Override
    public boolean insertSymbol(StandardSymbol symbol, Intersection at) {
        if (super.insertSymbol(symbol, at)) {
            if (currentSymbol == CIRCLE) boardView[at.getX()][at.getY()].setImage(circle);
            else boardView[at.getX()][at.getY()].setImage(cross);
            return true;
        }
        return false;
    }

    public void setDifficulty(Difficulty difficulty) {}
}