package ox5.game;

import ox5.enums.Direction;
import ox5.enums.State;
import ox5.game.elements.symbols.StandardSymbol;
import ox5.game.elements.Board;
import ox5.game.elements.Intersection;
import ox5.game.elements.symbols.WinningSymbol;

import static ox5.game.elements.symbols.StandardSymbol.CIRCLE;
import static ox5.game.elements.symbols.StandardSymbol.CROSS;

abstract class GameLogic {

    State state;
    StandardSymbol currentSymbol;
    private int move;
    Board board;

    private final static StandardSymbol[] FIVE_CIRCLES = { CIRCLE, CIRCLE, CIRCLE, CIRCLE, CIRCLE };
    private final static StandardSymbol[] FIVE_CROSSES = { CROSS, CROSS, CROSS, CROSS, CROSS };

    GameLogic() {
        board = new Board();
        state = State.RUNNING;
        currentSymbol = CIRCLE;
        move = 1;
    }

    boolean insertSymbol(StandardSymbol symbol, Intersection at) {
        return state == State.RUNNING && board.putSymbol(symbol, at);
    }

    private void detectVictory() {

        StandardSymbol[] pattern = currentSymbol == CIRCLE ? FIVE_CIRCLES : FIVE_CROSSES;

        for (int x = 0; x < Board.SIZE; x++) {
            for (int y = 0; y < Board.SIZE; y++) {
                for (Direction direction : Direction.values()) {
                    final Intersection startingPoint = Intersection.at(x, y);
                    if (board.contains(direction.shift(startingPoint, 4))
                            && board.patternFound(pattern, direction, startingPoint)) {
                        for (int i = 0; i < 5; i++) {
                            board.overwriteSymbol(WinningSymbol.of(currentSymbol), direction.shift(startingPoint, i));
                        }
                        state = State.win(currentSymbol);
                        return;
                    }
                }
            }
        }
    }

    void update() {
        detectVictory();
        if ((state == State.RUNNING) && (move >= Board.MAX_MOVES)) state = State.DRAW;
        currentSymbol = currentSymbol.other();
        move++;
    }
}