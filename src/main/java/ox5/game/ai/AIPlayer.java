package ox5.game.ai;

import ox5.enums.Difficulty;
import ox5.game.AIGame;
import ox5.game.ai.behavior.*;
import ox5.game.ai.random.Randomizer;
import ox5.game.elements.Board;
import ox5.game.elements.Intersection;
import ox5.game.elements.symbols.StandardSymbol;

import java.util.Optional;

public class AIPlayer {

    private final StandardSymbol aiSymbol;
    private final DefaultBehavior defaultBehavior;
    private final OptionalBehavior[] optionalBehaviors;
    private final AIGame game;

    public AIPlayer(Board board, Randomizer randomizer, StandardSymbol aiSymbol, Difficulty initialDifficulty, AIGame game) {
        this.aiSymbol = aiSymbol;
        this.defaultBehavior = new DefaultBehavior(randomizer, board);
        this.game = game;

        optionalBehaviors = new OptionalBehavior[] {
                new ReactingBehavior(randomizer, board, initialDifficulty, aiSymbol),
                new InitiatingBehavior(),
                new UnnecessaryBehavior()
        };
    }

    public void play() {

        boolean done = false;

        for (OptionalBehavior behavior : optionalBehaviors) {
            if (!done) {
                Optional<Intersection> intersection = behavior.findOptimalIntersection();
                if (intersection.isPresent()) {
                    done = game.insertSymbol(aiSymbol, intersection.get());
                }
            } else return;
        }

        if (!done) {
            final Intersection attractor = defaultBehavior.findOptimalIntersection();
            game.insertSymbol(aiSymbol, attractor);
        }
    }

    public void setDifficulty(Difficulty difficulty) {
        System.out.println("Setting difficulty: " + difficulty);
        for (OptionalBehavior behavior : optionalBehaviors) behavior.setDifficulty(difficulty);
    }
}
