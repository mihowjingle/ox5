package ox5.game.ai.behavior;

import lombok.RequiredArgsConstructor;
import ox5.game.ai.random.Randomizer;
import ox5.game.elements.Board;
import ox5.game.elements.Intersection;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import static ox5.game.elements.symbols.EmptySymbol.EMPTY;

@RequiredArgsConstructor
public class DefaultBehavior {

    private final Randomizer randomizer;
    private final Board board;

    private final static int ENOUGH_SAMPLES = 2; //maybe... for example...

    private Optional<Intersection> getAverageCoordinates() {

        final List<Intersection> coordinateList = new ArrayList<>();

        for (int x = 0; x < Board.SIZE; x++)
            for (int y = 0; y < Board.SIZE; y++) {
                if (board.getSymbolAt(x, y) != EMPTY) {
                    coordinateList.add(Intersection.at(x, y));
                }
            }

        final int count = coordinateList.size();

        if (count >= ENOUGH_SAMPLES) {
            final int xsum = coordinateList.stream().map(Intersection::getX).reduce(0, Integer::sum);
            final int ysum = coordinateList.stream().map(Intersection::getY).reduce(0, Integer::sum);

            return Optional.of(Intersection.at(xsum / count, ysum / count));
        }

        return Optional.empty();
    }

    private List<Intersection> getEmptyIntersections() {

        List<Intersection> intersections = new ArrayList<>();

        for (int x = 0; x < Board.SIZE; x++)
            for (int y = 0; y < Board.SIZE; y++)
                if (board.getSymbolAt(x, y) == EMPTY) {
                   intersections.add(Intersection.at(x, y));
                }

        return intersections;
    }

    private int distance(Intersection i1, Intersection i2) {
        return (i1.getX() - i2.getX()) * (i1.getX() - i2.getX()) + (i1.getY() - i2.getY()) * (i1.getY() - i2.getY());
    }

    public Intersection findOptimalIntersection() {

        //1. find "seed" x:y ("center of mass" of collective symbols), if it makes sense to take it into consideration, otherwise random
        //2. find spot on the board that is a) empty, b) closest to the seed x:y

        System.out.println("No known situation detected, choosing default behavior");

        Optional<Intersection> average = getAverageCoordinates();

        final Intersection seed = average
                .orElse(Intersection.at(randomizer.generateFromIntervalInclusive(6, 9), randomizer.generateFromIntervalInclusive(6, 9)));

        return getEmptyIntersections().stream()
                .min(Comparator.comparingInt(i -> distance(i, seed)))
                .orElseThrow(IllegalStateException::new);
    }
}
