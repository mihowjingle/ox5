package ox5.game.ai.behavior;

import ox5.enums.Difficulty;
import ox5.game.elements.Intersection;

import java.util.Optional;

public class InitiatingBehavior implements OptionalBehavior {

    @Override
    public Optional<Intersection> findOptimalIntersection() {

        //if no specific situations are detected, here we will try to maybe induce some

        return Optional.empty();
    }

    @Override
    public void setDifficulty(Difficulty difficulty) {
        //
    }
}
