package ox5.game.ai.behavior;

import ox5.enums.Difficulty;
import ox5.game.elements.Intersection;

import java.util.Optional;

public interface OptionalBehavior {

    Optional<Intersection> findOptimalIntersection();
    void setDifficulty(Difficulty difficulty);
}