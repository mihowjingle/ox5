package ox5.game.ai.behavior;

import lombok.RequiredArgsConstructor;
import lombok.Setter;
import ox5.enums.Difficulty;
import ox5.enums.SituationType;
import ox5.game.ai.random.Randomizer;
import ox5.game.elements.symbols.StandardSymbol;
import ox5.game.elements.Board;
import ox5.game.elements.Intersection;

import java.util.Optional;

public class ReactingBehavior implements OptionalBehavior {

    private final SituationSymbolPair[] situationSymbolPairs;
    private final Randomizer randomizer;
    private final Board board;
    private final StandardSymbol aiSymbol;

    @Setter
    private Difficulty difficulty;

    public ReactingBehavior(Randomizer randomizer, Board board, Difficulty initialDifficulty, StandardSymbol aiSymbol) {
        this.randomizer = randomizer;
        this.difficulty = initialDifficulty;
        this.board = board;
        this.aiSymbol = aiSymbol;
        StandardSymbol humanSymbol = aiSymbol.other();
        situationSymbolPairs = new SituationSymbolPair[] {
                new SituationSymbolPair(SituationType.FOUR_IN_LINE, aiSymbol),
                new SituationSymbolPair(SituationType.FOUR_IN_LINE, humanSymbol),
                new SituationSymbolPair(SituationType.THREE_IN_LINE_AI, aiSymbol),
                new SituationSymbolPair(SituationType.THREE_WITH_GAP, aiSymbol),
                new SituationSymbolPair(SituationType.DOUBLE_THREE_OR_MIXED, aiSymbol),
                new SituationSymbolPair(SituationType.DOUBLE_THREE_PLUS_ANOTHER, aiSymbol),
                new SituationSymbolPair(SituationType.TRIPLE_THREE, aiSymbol),
                new SituationSymbolPair(SituationType.THREE_IN_LINE_HUMAN, humanSymbol),
                new SituationSymbolPair(SituationType.THREE_WITH_GAP, humanSymbol),
                new SituationSymbolPair(SituationType.DOUBLE_THREE_OR_MIXED, humanSymbol),
                new SituationSymbolPair(SituationType.THREE_GAPS, aiSymbol),
                new SituationSymbolPair(SituationType.THREE_GAPS, humanSymbol),
                new SituationSymbolPair(SituationType.DOUBLE_THREE_PLUS_ANOTHER, humanSymbol),
                new SituationSymbolPair(SituationType.TRIPLE_THREE, humanSymbol),
                new SituationSymbolPair(SituationType.DOUBLE_TWO, aiSymbol),
                new SituationSymbolPair(SituationType.DOUBLE_TWO, humanSymbol),
                new SituationSymbolPair(SituationType.DOUBLE_ALMOST_THREE_GAPS, aiSymbol),
                new SituationSymbolPair(SituationType.DOUBLE_ALMOST_THREE_GAPS, humanSymbol),
                new SituationSymbolPair(SituationType.DOUBLE_MIXED_PLUS_ANOTHER, aiSymbol),
                new SituationSymbolPair(SituationType.DOUBLE_TWO_PLUS_ANOTHER, aiSymbol),
                new SituationSymbolPair(SituationType.TRIPLE_TWO, aiSymbol),
                new SituationSymbolPair(SituationType.DOUBLE_MIXED_PLUS_ANOTHER, humanSymbol),
                new SituationSymbolPair(SituationType.DOUBLE_TWO_PLUS_ANOTHER, humanSymbol),
                new SituationSymbolPair(SituationType.TRIPLE_TWO, humanSymbol)
        };
    }

    @Override
    public Optional<Intersection> findOptimalIntersection() {
        for (SituationSymbolPair pair : situationSymbolPairs) {
            if (randomizer.chance(difficulty.getChanceFor(pair.situationType))) {
                final Optional<Intersection> intersection = pair.situationType.detect(pair.symbol, board);
                if (intersection.isPresent()) {
                    log(pair, intersection.get());
                    return intersection;
                }
            }
        }
        return Optional.empty();
    }

    private void log(SituationSymbolPair pair, Intersection intersection) {
        System.out.println("Detected situation " + pair.situationType + " for " + pair.symbol + "/" +
                (pair.symbol == aiSymbol ? "AI" : "HUMAN") + " at " + intersection);imNotHere(pair);
    }

    @RequiredArgsConstructor
    private static class SituationSymbolPair {

        private final SituationType situationType;
        private final StandardSymbol symbol;
    }

    private final static String[] ANNOUNCEMENTS = {
            "Stop! Hammer time!",
            "Fresh new kicks and pants.",
            "I'm dope on the floor and I'm magic on the mic.",
            "Can't touch this!"
    };

    private void imNotHere(SituationSymbolPair pair) {
        if (pair.symbol == aiSymbol && pair.situationType == SituationType.FOUR_IN_LINE) {
            if (randomizer.chance(15)) {
                System.out.println(randomizer.takeOne(ANNOUNCEMENTS));
            }
        }
    }
}
