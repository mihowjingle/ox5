package ox5.game.ai.behavior;

import ox5.enums.Difficulty;
import ox5.game.elements.Intersection;

import java.util.Optional;

public class UnnecessaryBehavior implements OptionalBehavior {

    @Override
    public Optional<Intersection> findOptimalIntersection() {
        return Optional.empty();
    }

    @Override
    public void setDifficulty(Difficulty difficulty) {}
}
