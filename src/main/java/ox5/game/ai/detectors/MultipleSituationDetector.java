package ox5.game.ai.detectors;

import ox5.enums.Direction;
import ox5.game.elements.Board;
import ox5.game.elements.Intersection;
import ox5.game.elements.symbols.StandardSymbol;
import ox5.game.elements.symbols.Symbol;

import java.util.*;

import static java.util.stream.Collectors.toList;
import static ox5.game.elements.symbols.EmptySymbol.EMPTY;

public abstract class MultipleSituationDetector {

    protected boolean checkAnotherDirection(int x, int y, Direction ndirection, StandardSymbol symbol, Board board) {

        for (Direction direction : Direction.values()) {
            if (ndirection != direction) {
                if (almostThreeGaps(x, y, direction, symbol, board)
                        || threeInLine(x, y, direction, symbol, board)
                        || twoInLine(x, y, direction, symbol, board)) return true;
            }
        }

        return false;
    }

    protected boolean almostThreeGapsInAnotherLine(int x, int y, Direction ndirection, StandardSymbol symbol, Board board) {

        for (Direction direction : Direction.values()) {
            if (ndirection != direction) {
                if (almostThreeGaps(x, y, direction, symbol, board)) return true;
            }
        }

        return false;
    }

    protected boolean threeInAnotherLine(Intersection i, Direction ndirection, StandardSymbol symbol, Board board) {
        return threeInAnotherLine(i.getX(), i.getY(), ndirection, symbol, board);
    }

    protected boolean threeInAnotherLine(int x, int y, Direction ndirection, StandardSymbol symbol, Board board) {

        for (Direction direction : Direction.values()) {
            if (ndirection != direction) {
                if (threeInLine(x, y, direction, symbol, board)) return true;
            }
        }

        return false;
    }

    protected boolean twoInAnotherLine(Intersection i, Direction ndirection, StandardSymbol symbol, Board board) {
        return twoInAnotherLine(i.getX(), i.getY(), ndirection, symbol, board);
    }

    protected boolean twoInAnotherLine(int x, int y, Direction ndirection, StandardSymbol symbol, Board board) {

        for (Direction direction : Direction.values()) {
            if (ndirection != direction) {
                if (twoInLine(x, y, direction, symbol, board)) return true;
            }
        }

        return false;
    }

    protected boolean notTheSamePoint(int x1, int y1, int x2, int y2) {
        return x1 != x2 || y1 != y2;
    }

    protected boolean almostThreeGaps(int x, int y, Direction direction, StandardSymbol symbol, Board board) {

        Map<Integer, Symbol[]> shiftedPatterns = new HashMap<>();
        shiftedPatterns.put(0, new Symbol[] { EMPTY, EMPTY, symbol, EMPTY, symbol, EMPTY, symbol });
        shiftedPatterns.put(-2, new Symbol[] { symbol, EMPTY, EMPTY, EMPTY, symbol, EMPTY, symbol });
        shiftedPatterns.put(-4, new Symbol[] { symbol, EMPTY, symbol, EMPTY, EMPTY, EMPTY, symbol });
        shiftedPatterns.put(-6, new Symbol[] { symbol, EMPTY, symbol, EMPTY, symbol, EMPTY, EMPTY });

        for (Map.Entry<Integer, Symbol[]> entry : shiftedPatterns.entrySet()) {

            final Integer shift = entry.getKey();
            final Symbol[] pattern = entry.getValue();

            Intersection start = direction.shift(x, y, shift);
            if (board.contains(direction.shift(x, y, shift))
                    && board.contains(direction.shift(start.getX(), start.getY(), 6))
                    && board.patternFound(pattern, direction, direction.shift(x, y, shift))) {
                return true;
            }
        }

        return false;
    }

    protected boolean twoInLine(int x, int y, Direction direction, StandardSymbol symbol, Board board) {

        if (board.contains(direction.shift(x, y, -2))) {
            Intersection startingPoint = direction.shift(x, y, -2);
            if (board.contains(direction.shift(startingPoint, 5)))
                if (board.patternFound(new Symbol[] { EMPTY, symbol, EMPTY, EMPTY, symbol, EMPTY },
                        direction, startingPoint)) return true; // -+x-+-

            if (board.contains(direction.shift(startingPoint, 4)))
                if (board.patternFound(new Symbol[] { EMPTY, symbol, EMPTY, symbol, EMPTY },
                        direction, startingPoint)) return true; // -+x+-
        }

        if (board.contains(direction.shift(x, y, -3))) {
            Intersection startingPoint = direction.shift(x, y, -3);
            if (board.contains(direction.shift(startingPoint, 5)))
                if (board.patternFound(new Symbol[] { EMPTY, symbol, EMPTY, EMPTY, symbol, EMPTY },
                        direction, startingPoint)) return true; // -+-x+-

            if (board.contains(direction.shift(startingPoint, 4)))
                if (board.patternFound(new Symbol[] { EMPTY, symbol, symbol, EMPTY, EMPTY },
                        direction, startingPoint)) return true; // -++x-
        }

        if (board.contains(direction.shift(x, y, -1))) {
            Intersection startingPoint = direction.shift(x, y, -1);
            if (board.contains(direction.shift(startingPoint, 5))) {
                if (board.patternFound(new Symbol[] { EMPTY, EMPTY, EMPTY, symbol, symbol, EMPTY },
                        direction, startingPoint)) return true; // -x-++-

                if (board.patternFound(new Symbol[] { EMPTY, EMPTY, symbol, EMPTY, symbol, EMPTY },
                        direction, startingPoint)) return true; // -x+-+-
            }

            if (board.contains(direction.shift(startingPoint, 4)))
                if (board.patternFound(new Symbol[] { EMPTY, EMPTY, symbol, symbol, EMPTY },
                        direction, startingPoint)) return true; // -x++-
        }

        Intersection startingPoint = direction.shift(x, y, -4);
        if (board.contains(startingPoint)) if (board.contains(direction.shift(startingPoint, 5))) {
            if (board.patternFound(new Symbol[] { EMPTY, symbol, symbol, EMPTY, EMPTY, EMPTY },
                    direction, startingPoint)) return true; // -++-x-

            if (board.patternFound(new Symbol[] { EMPTY, symbol, EMPTY, symbol, EMPTY, EMPTY },
                    direction, startingPoint)) return true; // -+-+x-
        }

        return false;
    }

    protected boolean threeInLine(int x, int y, Direction direction, StandardSymbol symbol, Board board) {

        if (board.getSymbolAt(x, y) != EMPTY) return false;

        for (int startingShift = -4; startingShift <= 0; startingShift++) {
            if (board.contains(direction.shift(x, y, startingShift))
                    && board.contains(direction.shift(x, y, startingShift + 4))) {

                int numberOfSymbols = 0;
                int numberOfEmpty = 0;

                for (int iterationShift = 0; iterationShift <= 4; iterationShift++) {
                    if (board.getSymbolAt(direction.shift(x, y, startingShift + iterationShift)) == symbol) {
                        numberOfSymbols++;
                    }
                    else if (board.getSymbolAt(direction.shift(x, y, startingShift + iterationShift)) == EMPTY) {
                        numberOfEmpty++;
                    }
                    else break;
                }
                if (numberOfSymbols == 3 && numberOfEmpty == 2) return true;
            }
        }

        return false;
    }

    private boolean distanceExceeded(int maxDistance, Intersection... intersections) {

        for (int i = 0; i < intersections.length; i++)
            for (int j = i + 1; j < intersections.length; j++) {
                int distance = Math.max(
                        Math.abs(intersections[i].getX() - intersections[j].getX()),
                        Math.abs(intersections[i].getY() - intersections[j].getY())
                );
                if (distance > maxDistance) return true;
            }

        return false;
    }

    protected boolean foundEnoughSymbolsInYetAnotherLine(int x1, int y1, int x2, int y2, StandardSymbol symbol, Board board) {

        Intersection i1 = Intersection.at(x1, y1);
        Intersection i2 = Intersection.at(x2, y2);

        if (distanceExceeded(4, i1, i2)) return false;

        Optional<Direction> detectedDirection = Direction.between(i1, i2);
        if (detectedDirection.isPresent()) {
            
            final Direction direction = detectedDirection.get();

            if (direction != Direction.DOWN && i1.getX() > i2.getX()
                    || direction == Direction.DOWN && i1.getY() > i2.getY()) {
                Intersection t = i1;
                i1 = i2;
                i2 = t;
            }

            for (int startingShift = -4; startingShift <= 0; startingShift++) {
                if (board.contains(direction.shift(i1, startingShift))
                        && board.contains(direction.shift(i1, startingShift + 4))) {

                    int numberOfSymbols = 0;
                    int numberOfEmpty = 0;
                    List<Intersection> intersections = new ArrayList<>();
                    for (int iterationShift = 0; iterationShift <= 4; iterationShift++) {
                        final Intersection currentIntersection = direction.shift(i1, startingShift + iterationShift);
                        if (board.getSymbolAt(currentIntersection) == symbol) {
                            intersections.add(currentIntersection);
                            numberOfSymbols++;
                        }
                        else if (board.getSymbolAt(currentIntersection) == EMPTY) numberOfEmpty++;
                        else break;
                    }

                    intersections.add(i1);
                    intersections.add(i2);

                    if (numberOfSymbols == 1 && numberOfEmpty == 4 && board.getSymbolAt(direction.shift(i1, startingShift)) != symbol && board.getSymbolAt(direction.shift(i1, startingShift + 4)) != symbol) {
                        if (allInLineOfDirectionAndAtMostOneGapBetween(intersections).isPresent()) return true;
                    }
                    if (numberOfSymbols == 2 && numberOfEmpty == 3 && board.getSymbolAt(direction.shift(i1, startingShift)) != symbol && board.getSymbolAt(direction.shift(i1, startingShift + 4)) != symbol) {
                        if (allInLineOfDirectionAndAtMostOneGapBetween(intersections).isPresent()) return true;
                    }
                }
            }
        }

        return false;
    }

    private Optional<Direction> allInLineOfDirectionAndAtMostOneGapBetween(List<Intersection> intersections) {

        final Optional<Direction> detectedDirection = Direction.between(intersections);
        if (detectedDirection.isPresent()) {

            final Direction direction = detectedDirection.get();

            intersections = intersections.stream()
                    .sorted(direction::compare)
                    .collect(toList());

            int gapsFound = 0;
            for (int i = 0; i < intersections.size() - 1; i++) {
                gapsFound += gapsFound(intersections.get(i), intersections.get(i + 1));
                if (gapsFound > 1) return Optional.empty();
            }

            return detectedDirection;
        }

        return Optional.empty();
    }

    private int gapsFound(Intersection i1, Intersection i2) {

        final int xDistance = Math.abs(i1.getX() - i2.getX());
        final int yDistance = Math.abs(i1.getY() - i2.getY());

        return Math.max(xDistance, yDistance) - 1; //distance of 1 is 0 gaps - intersections are right next to each other
    }
}
