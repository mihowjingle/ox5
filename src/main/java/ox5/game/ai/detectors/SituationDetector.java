package ox5.game.ai.detectors;

import ox5.game.elements.symbols.StandardSymbol;
import ox5.game.elements.Board;
import ox5.game.elements.Intersection;

import java.util.Optional;

public interface SituationDetector {
    Optional<Intersection> detect(StandardSymbol symbol, Board board);
}
