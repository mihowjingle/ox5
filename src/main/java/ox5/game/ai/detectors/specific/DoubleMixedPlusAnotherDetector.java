package ox5.game.ai.detectors.specific;

import ox5.enums.Direction;
import ox5.game.elements.symbols.StandardSymbol;
import ox5.game.ai.detectors.MultipleSituationDetector;
import ox5.game.ai.detectors.SituationDetector;
import ox5.game.elements.Board;
import ox5.game.elements.Intersection;

import java.util.Optional;

public class DoubleMixedPlusAnotherDetector extends MultipleSituationDetector implements SituationDetector {

    @Override
    public Optional<Intersection> detect(StandardSymbol symbol, Board board) {

        for (int x = 0; x < Board.SIZE; x++)
            for (int y = 0; y < Board.SIZE; y++) {
                for (Direction direction : Direction.values()) {
                    Optional<Intersection> result = checkDiretion(x, y, direction, symbol, board);
                    if (result.isPresent()) return result;
                }
            }

        return Optional.empty();
    }

    private Optional<Intersection> checkDiretion(int x1, int y1, Direction direction, StandardSymbol symbol, Board board) {

        if (threeInLine(x1, y1, direction, symbol, board)) {
            for (int x2 = 0; x2 < Board.SIZE; x2++)
                for (int y2 = 0; y2 < Board.SIZE; y2++)
                    if (notTheSamePoint(x1, y1, x2, y2)) {
                        if (almostThreeGaps(x2, y2, direction, symbol, board) && direction.notTheSameLine(Intersection.at(x1, y1), Intersection.at(x2, y2))
                                || almostThreeGapsInAnotherLine(x2, y2, direction, symbol, board)
                                || twoInLine(x2, y2, direction, symbol, board) && direction.notTheSameLine(Intersection.at(x1, y1), Intersection.at(x2, y2))
                                || twoInAnotherLine(x2, y2, direction, symbol, board)) {
                            if (foundEnoughSymbolsInYetAnotherLine(x1, y1, x2, y2, symbol, board)) {
                                return Optional.of(Intersection.at(x2, y2));
                            }
                        }
                    }
        }

        return Optional.empty();
    }
}
