package ox5.game.ai.detectors.specific;

import ox5.enums.Direction;
import ox5.game.elements.symbols.StandardSymbol;
import ox5.game.ai.detectors.MultipleSituationDetector;
import ox5.game.ai.detectors.SituationDetector;
import ox5.game.elements.Board;
import ox5.game.elements.Intersection;

import java.util.Optional;

public class DoubleTwoDetector extends MultipleSituationDetector implements SituationDetector {

    @Override
    public Optional<Intersection> detect(StandardSymbol symbol, Board board) {

        for (int x = 0; x < Board.SIZE; x++)
            for (int y = 0; y < Board.SIZE; y++) {

                for (Direction direction : Direction.values()) {
                    if (twoInLine(x, y, direction, symbol, board)) {
                        if (twoInAnotherLine(x, y, direction, symbol, board)) {
                            return Optional.of(Intersection.at(x, y));
                        }
                    }
                }
            }

        return Optional.empty();
    }
}
