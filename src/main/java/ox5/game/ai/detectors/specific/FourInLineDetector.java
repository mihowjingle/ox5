package ox5.game.ai.detectors.specific;

import ox5.enums.Direction;
import ox5.game.ai.detectors.SituationDetector;
import ox5.game.elements.Board;
import ox5.game.elements.Intersection;
import ox5.game.elements.symbols.StandardSymbol;

import java.util.Optional;

import static ox5.game.elements.symbols.EmptySymbol.EMPTY;

public class FourInLineDetector implements SituationDetector {

    @Override
    public Optional<Intersection> detect(StandardSymbol symbol, Board board) {

        for (int x = 0; x < Board.SIZE; x++)
            for (int y = 0; y < Board.SIZE; y++)
                if ((board.getSymbolAt(x, y) == EMPTY) || (board.getSymbolAt(x, y) == symbol)) {
                    for (Direction direction : Direction.values()) {
                        Optional<Intersection> result = checkDirection(symbol, board, x, y, direction);
                        if (result.isPresent()) return result;
                    }
                }
        return Optional.empty();
    }

    private Optional<Intersection> checkDirection(StandardSymbol symbol, Board board, int x, int y, Direction direction) {

        if (board.contains(direction.shift(x, y, 4))) {

            int symbolsFound = 0;
            Optional<Intersection> gap = Optional.empty();

            for (int shift = 0; shift < 5; shift++) {

                final Intersection intersection = direction.shift(x, y, shift);

                if (board.getSymbolAt(intersection) == symbol) symbolsFound++;
                else if (board.getSymbolAt(intersection) == EMPTY) {
                    if (gap.isPresent()) break;
                    else {
                        gap = Optional.of(intersection);
                    }
                } else break;
            }
            if ((symbolsFound == 4) && gap.isPresent()) return gap;
        }
        return Optional.empty();
    }
}
