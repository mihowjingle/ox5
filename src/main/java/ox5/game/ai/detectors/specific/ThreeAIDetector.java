package ox5.game.ai.detectors.specific;

import ox5.enums.Direction;
import ox5.game.elements.symbols.StandardSymbol;
import ox5.game.ai.detectors.SituationDetector;
import ox5.game.elements.Board;
import ox5.game.elements.Intersection;

import java.util.Optional;

import static ox5.game.elements.symbols.EmptySymbol.EMPTY;

public class ThreeAIDetector implements SituationDetector {

    @Override
    public Optional<Intersection> detect(StandardSymbol aiSymbol, Board board) {

        for (int x = 0; x < Board.SIZE; x++)
            for (int y = 0; y < Board.SIZE; y++)
                if (board.getSymbolAt(x, y) == EMPTY) {
                    for (Direction direction : Direction.values()) {
                        Optional<Intersection> result = checkDirection(aiSymbol, board, direction, x, y);
                        if (result.isPresent()) return result;
                    }
                }
        return Optional.empty();
    }

    private Optional<Intersection> checkDirection(StandardSymbol aiSymbol, Board board, Direction direction, int x, int y) {

        StandardSymbol humanSymbol = aiSymbol.other();

        if (board.contains(direction.shift(x, y, 4))) {
            if (board.getSymbolAt(direction.shift(x, y, 1)) == aiSymbol
            && board.getSymbolAt(direction.shift(x, y, 2)) == aiSymbol
            && board.getSymbolAt(direction.shift(x, y, 3)) == aiSymbol
            && board.getSymbolAt(direction.shift(x, y, 4)) == EMPTY) {
                if (board.contains(direction.shift(x, y, 5))
                        && board.getSymbolAt(direction.shift(x, y, 5)) != humanSymbol) {
                    return Optional.of(direction.shift(x, y, 4));
                }
                return Optional.of(Intersection.at(x, y));
            }
        }
        return Optional.empty();
    }
}
