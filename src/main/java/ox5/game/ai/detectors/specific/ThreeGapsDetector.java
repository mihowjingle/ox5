package ox5.game.ai.detectors.specific;

import ox5.enums.Direction;
import ox5.game.ai.detectors.SituationDetector;
import ox5.game.elements.Board;
import ox5.game.elements.Intersection;
import ox5.game.elements.symbols.StandardSymbol;
import ox5.game.elements.symbols.Symbol;

import java.util.Optional;

import static ox5.game.elements.symbols.EmptySymbol.EMPTY;

public class ThreeGapsDetector implements SituationDetector {

    @Override
    public Optional<Intersection> detect(StandardSymbol symbol, Board board) {

        Symbol[] pattern = { symbol, EMPTY, symbol, EMPTY, symbol, EMPTY, symbol };

        for (int x = 0; x < Board.SIZE; x++)
            for (int y = 0; y < Board.SIZE; y++) {
                for (Direction direction : Direction.values()) {
                    if (board.contains(direction.shift(x, y, 6))) {
                        if (board.patternFound(pattern, direction, x, y))
                            return Optional.of(direction.shift(x, y,3));
                    }
                }
            }
        return Optional.empty();
    }
}
