package ox5.game.ai.detectors.specific;

import lombok.RequiredArgsConstructor;
import ox5.enums.Direction;
import ox5.game.elements.symbols.StandardSymbol;
import ox5.game.ai.detectors.SituationDetector;
import ox5.game.ai.random.Randomizer;
import ox5.game.elements.Board;
import ox5.game.elements.Intersection;

import java.util.Optional;

import static ox5.game.elements.symbols.EmptySymbol.EMPTY;

@RequiredArgsConstructor
public class ThreeHumanDetector implements SituationDetector {

    private final Randomizer randomizer;

    @Override
    public Optional<Intersection> detect(StandardSymbol humanSymbol, Board board) {

        for (int x = 0; x < Board.SIZE; x++)
            for (int y = 0; y < Board.SIZE; y++)
                if (board.getSymbolAt(x, y) == EMPTY) {
                    for (Direction direction : Direction.values()) {
                        Optional<Intersection> result = checkDirection(humanSymbol, board, direction, x, y);
                        if (result.isPresent()) return result;
                    }
                }
        return Optional.empty();
    }
    
    private Optional<Intersection> checkDirection(StandardSymbol humanSymbol, Board board, Direction direction, int x, int y) {
        
        StandardSymbol aiSymbol = humanSymbol.other();

        if (board.contains(direction.shift(x, y, 4))) {
            if (board.getSymbolAt(direction.shift(x, y, 1)) == humanSymbol
            && board.getSymbolAt(direction.shift(x, y, 2)) == humanSymbol
            && board.getSymbolAt(direction.shift(x, y, 3)) == humanSymbol
            && board.getSymbolAt(direction.shift(x, y, 4)) == EMPTY) {
                Optional<Intersection> result = chooseMoreOccupiedArea(humanSymbol, board, direction, x, y, false);
                if (result.isPresent()) return result;
                return chooseMoreOccupiedArea(aiSymbol, board, direction, x, y, true);
            }
        }
        return Optional.empty();
    }
    
    private Optional<Intersection> chooseMoreOccupiedArea(StandardSymbol symbol, Board board, Direction direction, int x, int y, boolean ifIndefiniteThenAny) {
        
        int c0 = countNeighbouringSymbols(symbol, board, x, y);
        int c4 = countNeighbouringSymbols(symbol, board, direction.shift(x, y, 4));
        if (c0 > c4) {
            return Optional.of(Intersection.at(x, y));
        }
        if (c0 < c4) {
            return Optional.of(direction.shift(x, y, 4));
        }
        if (ifIndefiniteThenAny) {
            return randomizer.chance(50) ?
                    Optional.of(Intersection.at(x, y)) : Optional.of(direction.shift(x, y, 4));
        }
        return Optional.empty();
    }

    private int countNeighbouringSymbols(StandardSymbol symbol, Board board, int x, int y) {
        int count = 0;
        for (int dx = -2; dx < 3; dx++)
            for (int dy = -2; dy < 3; dy++) {
                if (board.contains(x + dx, y + dy)) if (board.getSymbolAt(x + dx, y + dy) == symbol) count++;
            }
        return count;
    }

    private int countNeighbouringSymbols(StandardSymbol symbol, Board board, Intersection intersection) {
        return countNeighbouringSymbols(symbol, board, intersection.getX(), intersection.getY());
    }
}
