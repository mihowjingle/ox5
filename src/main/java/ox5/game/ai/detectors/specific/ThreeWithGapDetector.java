package ox5.game.ai.detectors.specific;

import ox5.enums.Direction;
import ox5.game.ai.detectors.SituationDetector;
import ox5.game.elements.Board;
import ox5.game.elements.Intersection;
import ox5.game.elements.symbols.StandardSymbol;
import ox5.game.elements.symbols.Symbol;

import java.util.Optional;

import static ox5.game.elements.symbols.EmptySymbol.EMPTY;

public class ThreeWithGapDetector implements SituationDetector {

    private Symbol[] pattern, reversedPattern;

    @Override
    public Optional<Intersection> detect(StandardSymbol symbol, Board board) {

        pattern = new Symbol[] { EMPTY, symbol, symbol, EMPTY, symbol, EMPTY };
        reversedPattern = new Symbol[] { EMPTY, symbol, EMPTY, symbol, symbol, EMPTY };

        for (int x = 0; x < Board.SIZE; x++)
            for (int y = 0; y < Board.SIZE; y++) {
                for (Direction direction : Direction.values()) {
                    Optional<Intersection> result = checkDirection(board, x, y, direction);
                    if (result.isPresent()) return result;
                }
            }

        return Optional.empty();
    }

    private Optional<Intersection> checkDirection(Board board, int x, int y, Direction direction) {

        if (board.contains(direction.shift(x, y, 5))) {
            if (board.patternFound(pattern, direction, x, y)) {
                return Optional.of(direction.shift(x, y, 3));
            }
            if (board.patternFound(reversedPattern, direction, x, y)) {
                return Optional.of(direction.shift(x, y, 2));
            }
        }

        return Optional.empty();
    }
}
