package ox5.game.ai.detectors.specific;

import lombok.RequiredArgsConstructor;
import ox5.enums.Direction;
import ox5.game.ai.detectors.MultipleSituationDetector;
import ox5.game.ai.detectors.SituationDetector;
import ox5.game.ai.random.Randomizer;
import ox5.game.elements.Board;
import ox5.game.elements.Intersection;
import ox5.game.elements.symbols.StandardSymbol;
import ox5.game.elements.symbols.Symbol;

import java.util.Optional;

import static ox5.game.elements.symbols.EmptySymbol.EMPTY;

@RequiredArgsConstructor
public class TripleThreeDetector extends MultipleSituationDetector implements SituationDetector {

    private final Randomizer randomizer;
    
    @Override
    public Optional<Intersection> detect(StandardSymbol symbol, Board board) {

        Symbol[] pattern = { EMPTY, EMPTY, EMPTY, EMPTY, EMPTY };

        for (int x = 0; x < Board.SIZE; x++) for (int y = 0; y < Board.SIZE; y++) {
            for (Direction direction : Direction.values()) {
                if (board.contains(direction.shift(x, y, 4)) && board.patternFound(pattern, direction, x, y)) {
                    final Optional<Intersection> intersection = findTripleThreeInAnotherLine(x, y, direction, symbol, board);
                    if (intersection.isPresent()) return intersection;
                }
            }
        }

        return Optional.empty();
    }

    private Optional<Intersection> findTripleThreeInAnotherLine(int x, int y, Direction direction, StandardSymbol symbol, Board board) {
        for (int a = 0; a < 3; a++)
            for (int b = a + 1; b < 4; b++)
                for (int c = b + 1; c < 5; c++) {
                    final Intersection i1 = direction.shift(x, y, a);
                    if (threeInAnotherLine(i1, direction, symbol, board)) {
                        final Intersection i2 = direction.shift(x, y, b);
                        if (threeInAnotherLine(i2, direction, symbol, board)) {
                            final Intersection i3 = direction.shift(x, y, c);
                            if (threeInAnotherLine(i3, direction, symbol, board)) {
                                return Optional.of(randomizer.takeOne(new Intersection[] { i1, i2, i3 }));
                            }
                        }
                    }
                }

        return Optional.empty();
    }
}
