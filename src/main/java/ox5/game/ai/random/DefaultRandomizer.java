package ox5.game.ai.random;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Random;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class DefaultRandomizer implements Randomizer {

    private static final DefaultRandomizer INSTANCE;

    private Random random = new Random();

    static {
        INSTANCE = new DefaultRandomizer();
    }

    public static DefaultRandomizer get() {
        return INSTANCE;
    }

    @Override
    public boolean chance(int percent) {
        int r = random.nextInt(100); // 0 - 99
        return r < percent;
    }

    @Override
    public <T> T takeOne(T[] ts) {
        int i = random.nextInt(ts.length);
        return ts[i];
    }

    @Override
    public int generateFromIntervalInclusive(int min, int max) {
        return random.nextInt(max - min + 1) + min;
    }
}