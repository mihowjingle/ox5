package ox5.game.ai.random;

public interface Randomizer {
    boolean chance(int percent);
    <T> T takeOne(T[] ts);
    int generateFromIntervalInclusive(int min, int max);
}