package ox5.game.elements;

import ox5.enums.Direction;
import ox5.game.elements.symbols.StandardSymbol;
import ox5.game.elements.symbols.Symbol;
import ox5.game.elements.symbols.WinningSymbol;

import static ox5.game.elements.symbols.EmptySymbol.EMPTY;

public class Board {

    public static final int SIZE = 16;
    public static final int MAX_MOVES = 256;

    protected Symbol[][] symbols;

    public Board() {
        symbols = new Symbol[SIZE][SIZE];
        for (int x = 0; x < SIZE; x++)
            for (int y = 0; y < SIZE; y++)
                symbols[x][y] = EMPTY;
    }

    public boolean putSymbol(StandardSymbol symbol, Intersection at) {
        if (symbols[at.getX()][at.getY()] == EMPTY) {
            symbols[at.getX()][at.getY()] = symbol;
            return true;
        }
        return false;
    }

    public void overwriteSymbol(WinningSymbol symbol, Intersection at) {
        symbols[at.getX()][at.getY()] = symbol;
    }

    public Symbol getSymbolAt(Intersection at) {
        return symbols[at.getX()][at.getY()];
    }

    public Symbol getSymbolAt(int x, int y) {
        return symbols[x][y];
    }

    public boolean patternFound(Symbol[] pattern, Direction direction, Intersection startingPoint) {
        return patternFound(pattern, direction, startingPoint.getX(), startingPoint.getY());
    }

    public boolean patternFound(Symbol[] pattern, Direction direction, int x, int y) {
        for (int shift = 0; shift < pattern.length; shift++) {
            if (getSymbolAt(direction.shift(x, y, shift)) != pattern[shift]) return false;
        }
        return true;
    }

    public boolean contains(Intersection at) {
        return contains(at.getX(), at.getY());
    }

    public boolean contains(int x, int y) {
        return x >= 0 && x < SIZE && y >= 0 && y < SIZE;
    }
}
