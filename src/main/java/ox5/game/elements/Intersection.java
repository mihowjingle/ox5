package ox5.game.elements;

import lombok.*;

@Getter
@ToString
@EqualsAndHashCode
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class Intersection {

    private final int x;
    private final int y;

    public static Intersection at(int x, int y) {
        return new Intersection(x, y);
    }
}
