package ox5.game.elements.symbols;

public enum StandardSymbol implements Symbol {

    CIRCLE,
    CROSS;

    public StandardSymbol other() {
        return this == CIRCLE ? CROSS : CIRCLE;
    }
}
