package ox5.game.elements.symbols;

import static ox5.game.elements.symbols.StandardSymbol.CIRCLE;

public enum WinningSymbol implements Symbol {

    WINNING_CIRCLE,
    WINNING_CROSS;

    public static WinningSymbol of(StandardSymbol symbol) {
        return symbol == CIRCLE ? WINNING_CIRCLE : WINNING_CROSS;
    }
}
