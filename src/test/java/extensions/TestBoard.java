package extensions;

import ox5.game.elements.Board;
import ox5.game.elements.symbols.Symbol;

import static ox5.game.elements.symbols.EmptySymbol.EMPTY;
import static ox5.game.elements.symbols.StandardSymbol.CIRCLE;
import static ox5.game.elements.symbols.StandardSymbol.CROSS;

public class TestBoard extends Board {

    public TestBoard(String pattern) {
        final int size = Board.SIZE * Board.SIZE;
        if (pattern.length() != size) {
            throw new IllegalArgumentException("Pattern size must be exactly " + size);
        }
        for (int x = 0; x < Board.SIZE; x++)
            for (int y = 0; y < Board.SIZE; y++) {
                Symbol symbol = fromChar(pattern.charAt(x * Board.SIZE + y));
                symbols[y][x] = symbol;
            }
    }

    private Symbol fromChar(char c) {
        if (c == 'o') return CIRCLE;
        if (c == 'x') return CROSS;
        if (c == '-') return EMPTY;
        throw new IllegalArgumentException("Unrecognized symbol");
    }
}
