package tests;

import org.junit.jupiter.api.Test;
import ox5.enums.Direction;
import ox5.game.elements.Intersection;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class DirectionDetectionTests {

    // 2-argument method

    @Test
    void shouldNotDetectSamePoint() {
        assertFalse(Direction.between(
                Intersection.at(2, 2),
                Intersection.at(2, 2)
        ).isPresent(), "Same point, only repeated, should not detect!");
    }

    // list argument method

    @Test
    void shouldNotDetect1() {
        assertFalse(Direction.between(Arrays.asList(
                Intersection.at(2, 2),
                Intersection.at(2, 2),
                Intersection.at(2, 2)
        )).isPresent(), "Same point, only repeated, should not detect!");
    }

    @Test
    void shouldNotDetect2() {
        assertFalse(Direction.between(Arrays.asList(
                Intersection.at(2, 2),
                Intersection.at(3, 3),
                Intersection.at(3, 4)
        )).isPresent(), "Not a straight line!");
    }

    @Test
    void shouldNotDetect3() {
        assertFalse(Direction.between(Arrays.asList(
                Intersection.at(2, 3),
                Intersection.at(3, 4),
                Intersection.at(4, 6),
                Intersection.at(5, 7)
        )).isPresent(), "Two lines separately, yes, but all 4 are not in line!");
    }

    @Test
    void shouldNotDetect4() {
        assertFalse(Direction.between(Arrays.asList(
                Intersection.at(2, 3),
                Intersection.at(2, 4),
                Intersection.at(3, 6),
                Intersection.at(3, 7)
        )).isPresent(), "Two lines separately, yes, but all 4 are not in line!");
    }

    @Test
    void shouldNotDetect5() {
        assertFalse(Direction.between(Arrays.asList(
                Intersection.at(2, 3),
                Intersection.at(2, 4),
                Intersection.at(4, 6),
                Intersection.at(4, 7)
        )).isPresent(), "Two lines separately, yes, but all 4 are not in line!");
    }

    @Test
    void shouldNotDetect6() {
        assertFalse(Direction.between(Arrays.asList(
                Intersection.at(2, 5),
                Intersection.at(2, 6),
                Intersection.at(3, 7),
                Intersection.at(2, 8)
        )).isPresent(), "Odd point, should not detect!");
    }

    @Test
    void shouldNotDetect7() {
        assertFalse(Direction.between(Arrays.asList(
                Intersection.at(2, 5),
                Intersection.at(2, 6),
                Intersection.at(1, 7)
        )).isPresent(), "Not a straight line!");
    }

    @Test
    void shouldNotDetect8() {
        assertFalse(Direction.between(Arrays.asList(
                Intersection.at(1, 7),
                Intersection.at(2, 6),
                Intersection.at(2, 5)
        )).isPresent(), "Not a straight line!");
    }

    @Test
    void shouldNotDetect9() {
        assertFalse(Direction.between(Arrays.asList(
                Intersection.at(2, 5),
                Intersection.at(2, 6),
                Intersection.at(1, 8)
        )).isPresent(), "Not a straight line!");
    }

    @Test
    void shouldNotDetect10() {
        assertFalse(Direction.between(Arrays.asList(
                Intersection.at(1, 8),
                Intersection.at(2, 6),
                Intersection.at(2, 5)
        )).isPresent(), "Not a straight line!");
    }

    @Test
    void shouldDetectRightUp() {
        final Optional<Direction> direction = Direction.between(Arrays.asList(
                Intersection.at(9, 6),
                Intersection.at(8, 7),
                Intersection.at(7, 8),
                Intersection.at(6, 9)
        ));
        assertTrue(direction.isPresent());
        assertSame(direction.get(), Direction.RIGHT_UP);
    }

    @Test
    void shouldDetectRight() {
        final Optional<Direction> direction = Direction.between(Arrays.asList(
                Intersection.at(1, 1),
                Intersection.at(2, 1),
                Intersection.at(3, 1),
                Intersection.at(4, 1)
        ));
        assertTrue(direction.isPresent());
        assertSame(direction.get(), Direction.RIGHT);
    }

    @Test
    void shouldDetectRightDown() {
        final Optional<Direction> direction = Direction.between(Arrays.asList(
                Intersection.at(1, 1),
                Intersection.at(2, 2),
                Intersection.at(3, 3),
                Intersection.at(4, 4)
        ));
        assertTrue(direction.isPresent());
        assertSame(direction.get(), Direction.RIGHT_DOWN);
    }

    @Test
    void shouldDetectDown() {
        final Optional<Direction> direction = Direction.between(Arrays.asList(
                Intersection.at(1, 1),
                Intersection.at(1, 2),
                Intersection.at(1, 3),
                Intersection.at(1, 4)
        ));
        assertTrue(direction.isPresent());
        assertSame(direction.get(), Direction.DOWN);
    }

    @Test
    void orderShouldNotMatter() {
        assertEquals(Direction.between(Arrays.asList( //both
                Intersection.at(2, 5),
                Intersection.at(2, 6),
                Intersection.at(2, 8)
        )).isPresent(), Direction.between(Arrays.asList(
                Intersection.at(2, 5),
                Intersection.at(2, 8),
                Intersection.at(2, 6)
        )).isPresent());
        assertEquals(Direction.between(Arrays.asList( //neither
                Intersection.at(2, 5),
                Intersection.at(2, 6),
                Intersection.at(1, 8)
        )).isPresent(), Direction.between(Arrays.asList(
                Intersection.at(2, 5),
                Intersection.at(1, 8),
                Intersection.at(2, 6)
        )).isPresent());
    }
}
