package tests;

import extensions.TestBoard;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import ox5.enums.SituationType;
import ox5.game.elements.symbols.StandardSymbol;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PlusAnotherSituationTests {

    @Test
    void shouldNotDetectCircleOnEmptyBoard() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertFalse(SituationType.DOUBLE_THREE_PLUS_ANOTHER.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldNotDetectCrossOnEmptyBoard() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertFalse(SituationType.DOUBLE_THREE_PLUS_ANOTHER.detect(StandardSymbol.CROSS, testBoard).isPresent());
    }

    @Test
    void shouldNotDetectSymbolTooFarWouldBeSixth() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "-------o--o-----" +
                "------ooox------" +
                "-------xxxo-----" +
                "-------x--------" +
                "----------------" +
                "-------x--------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertFalse(SituationType.DOUBLE_THREE_PLUS_ANOTHER.detect(StandardSymbol.CROSS, testBoard).isPresent());
    }

    @Test
    void shouldNotDetectTooWideGap1() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "-----o-o-x------" +
                "-----oxxx-------" +
                "-----xox--------" +
                "-----ooxx-------" +
                "-----o-o--------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertFalse(SituationType.DOUBLE_THREE_PLUS_ANOTHER.detect(StandardSymbol.CROSS, testBoard).isPresent());
    }

    @Test
    void shouldNotDetectTooWideGap2() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "------x---------" +
                "-------x--------" +
                "------xo--------" +
                "-----xox--------" +
                "----o--ox-------" +
                "-------oxo------" +
                "--------o-------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertFalse(SituationType.DOUBLE_THREE_PLUS_ANOTHER.detect(StandardSymbol.CROSS, testBoard).isPresent());
    }

    @Test
    void shouldNotDetectTooWideGap3() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "-------o--------" +
                "--o---x---------" +
                "---xoxo---------" +
                "----x-o--o------" +
                "-----x----------" +
                "----x-----------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertFalse(SituationType.DOUBLE_THREE_PLUS_ANOTHER.detect(StandardSymbol.CROSS, testBoard).isPresent());
    }

    @Test
    void shouldNotDetectTooWideGap4() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "------o---------" +
                "-o---x----------" +
                "--xoxoo---------" +
                "---x------------" +
                "----x---o-------" +
                "---x------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertFalse(SituationType.DOUBLE_THREE_PLUS_ANOTHER.detect(StandardSymbol.CROSS, testBoard).isPresent());
    }

    @Test
    void shouldNotDetectTooWideGap5() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "-----x---x------" +
                "------o-o-------" +
                "-------o--------" +
                "------o-o-------" +
                "-------o--------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertFalse(SituationType.DOUBLE_THREE_PLUS_ANOTHER.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldNotDetectTooWideGap6() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----x-----------" +
                "-----oxo--------" +
                "-----xo-o-------" +
                "-----oxo-x------" +
                "----x-----------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertFalse(SituationType.DOUBLE_THREE_PLUS_ANOTHER.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldNotDetectTooWideGap7() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------x-----" +
                "-----x---o------" +
                "------o-o-------" +
                "-------o--------" +
                "--------o-------" +
                "-------o--------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertFalse(SituationType.DOUBLE_THREE_PLUS_ANOTHER.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldNotDetectSymbolInAnotherLineTooFar2() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "---------x------" +
                "------xoo-------" +
                "-------ox-------" +
                "------o---------" +
                "---o-----x------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertFalse(SituationType.DOUBLE_MIXED_PLUS_ANOTHER.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldNotDetectTooWideAndBlockedByOtherSymbol() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "--------o--o----" +
                "---------x------" +
                "------ooxxx-----" +
                "------ooxx------" +
                "------xxxo------" +
                "-----xooo-------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertFalse(SituationType.DOUBLE_THREE_PLUS_ANOTHER.detect(StandardSymbol.CROSS, testBoard).isPresent());
    }

    @Test
    void shouldDetectGoodPattern1() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------o-----" +
                "------xooo------" +
                "------xooo------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertTrue(SituationType.DOUBLE_THREE_PLUS_ANOTHER.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldDetectGoodPattern2() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "--------o-------" +
                "----------------" +
                "----xooo--------" +
                "----xooo--------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertTrue(SituationType.DOUBLE_THREE_PLUS_ANOTHER.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldNotDetectSymbolInAnotherLineTooFar1() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "--------o-------" +
                "----------------" +
                "----------------" +
                "----xooo--------" +
                "----xooo--------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertFalse(SituationType.DOUBLE_THREE_PLUS_ANOTHER.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    @Disabled
    void shouldNotDetectSymbolInAnotherLineTooFar3() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "-------xxo------" +
                "-------oxo------" +
                "-----ooxxx------" +
                "-----ooxo-o-----" +
                "---oxxxxo-------" +
                "-----o-o--------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertFalse(SituationType.DOUBLE_TWO_PLUS_ANOTHER.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldDetectGoodPattern18() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----ooox--------" +
                "----ooxo--------" +
                "----xxxo--------" +
                "----o-x---------" +
                "------xx--------" +
                "------oo--------" +
                "-----xxxoo------" +
                "--------x-------" +
                "----------------"
        );
        assertTrue(SituationType.DOUBLE_TWO_PLUS_ANOTHER.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldDetectGoodPattern3() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "------oo--o-----" +
                "------ooox------" +
                "-------xxxo-----" +
                "------xx--------" +
                "----------------" +
                "-------x--------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertTrue(SituationType.DOUBLE_THREE_PLUS_ANOTHER.detect(StandardSymbol.CROSS, testBoard).isPresent());
    }

    @Test
    void shouldDetectGoodPattern4() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "------oo--o-----" +
                "------oooxx-----" +
                "-------xxxo-----" +
                "------xx--------" +
                "-------o--------" +
                "-------x--------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertTrue(SituationType.DOUBLE_THREE_PLUS_ANOTHER.detect(StandardSymbol.CROSS, testBoard).isPresent());
    }

    @Test
    void shouldDetectGoodPattern5() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "-----oxx-x------" +
                "-----xxoo-------" +
                "------xo--------" +
                "------oo--------" +
                "------o---------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertTrue(SituationType.DOUBLE_THREE_PLUS_ANOTHER.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldDetectGoodPattern6() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "-----xxo--------" +
                "----xxoo--------" +
                "----oooxo-------" +
                "----x-----------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertTrue(SituationType.DOUBLE_MIXED_PLUS_ANOTHER.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldDetectGoodPattern7() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "------x---------" +
                "-------o--------" +
                "----xxo-o-------" +
                "-----oxx-o-o----" +
                "----x-o-x-------" +
                "-------o--------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertTrue(SituationType.DOUBLE_THREE_PLUS_ANOTHER.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldDetectGoodPattern8() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "------x---------" +
                "-----xx---------" +
                "-----xooo-------" +
                "-------oo-------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertTrue(SituationType.DOUBLE_MIXED_PLUS_ANOTHER.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldDetectGoodPattern9() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "-----xooo-------" +
                "----------------" +
                "---------o------" +
                "-----xooo-------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertTrue(SituationType.DOUBLE_THREE_PLUS_ANOTHER.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldDetectGoodPattern10() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "-----xooo-------" +
                "---------o------" +
                "----------------" +
                "-------oo-------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertTrue(SituationType.DOUBLE_MIXED_PLUS_ANOTHER.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldDetectGoodPattern11() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "-----xooo-------" +
                "----------------" +
                "---------o------" +
                "---------o------" +
                "-----xooo-------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertTrue(SituationType.DOUBLE_THREE_PLUS_ANOTHER.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldDetectGoodPattern12() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "-----xooo-------" +
                "---------o------" +
                "----------------" +
                "---------o------" +
                "-------oo-------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertTrue(SituationType.DOUBLE_MIXED_PLUS_ANOTHER.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldDetectGoodPattern13() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "-----xooo-------" +
                "---------o------" +
                "----------------" +
                "---------o------" +
                "-----xooo-------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertTrue(SituationType.DOUBLE_THREE_PLUS_ANOTHER.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldDetectGoodPattern14() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "-----xooo-------" +
                "----------------" +
                "---------o------" +
                "---------o------" +
                "-------oo-------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertTrue(SituationType.DOUBLE_MIXED_PLUS_ANOTHER.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldNotDetectTheTwoAreBlocked() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "-----xooo-------" +
                "--------xo------" +
                "--------x-------" +
                "------xoo-------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertFalse(SituationType.DOUBLE_MIXED_PLUS_ANOTHER.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldNotDetectTheOtherLineIsBlocked1() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "-----------x----" +
                "--------x-o-----" +
                "---------oox----" +
                "------xxoxo-----" +
                "-----oxooox-----" +
                "----xxxooox-----" +
                "----oxooxo------" +
                "-----xxxo-------" +
                "-------o--------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertFalse(SituationType.DOUBLE_THREE_PLUS_ANOTHER.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldNotDetectTheOtherLineIsBlocked2() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "------xx--------" +
                "------oo--------" +
                "------oo--------" +
                "------oo--------" +
                "----ox----------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertFalse(SituationType.DOUBLE_THREE_PLUS_ANOTHER.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldDetectGoodPattern15() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "-----xooo-------" +
                "---------o------" +
                "----------------" +
                "------xoo-------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertTrue(SituationType.DOUBLE_MIXED_PLUS_ANOTHER.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldDetectGoodPattern16() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "-----xooo-------" +
                "---------o------" +
                "----------------" +
                "--------o-------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertTrue(SituationType.DOUBLE_MIXED_PLUS_ANOTHER.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldNotDetectNotEnoughSymbols() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "-----xooo-------" +
                "--------xo------" +
                "----------------" +
                "--------o-------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertFalse(SituationType.DOUBLE_MIXED_PLUS_ANOTHER.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldDetectGoodPattern17() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "-------x--------" +
                "------xxoo------" +
                "------ooox------" +
                "------xooo------" +
                "-----xxox-------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertTrue(SituationType.DOUBLE_THREE_PLUS_ANOTHER.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }
}