package tests;

import extensions.TestBoard;
import org.junit.jupiter.api.Test;
import ox5.enums.SituationType;
import ox5.game.elements.symbols.StandardSymbol;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TripleThreeTests {

    @Test
    void shouldDetect1() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "------xooo------" +
                "------xoox------" +
                "------xoox------" +
                "------xxxx------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertTrue(SituationType.TRIPLE_THREE.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldDetect2() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "------xxxx------" +
                "------xooo------" +
                "------xooo------" +
                "------xxxo------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertTrue(SituationType.TRIPLE_THREE.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldDetect3() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "------xxxx------" +
                "------xoox------" +
                "------xoox------" +
                "------ooox------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertTrue(SituationType.TRIPLE_THREE.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldDetect4() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "------oxxx------" +
                "------ooox------" +
                "------ooox------" +
                "------xxxx------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertTrue(SituationType.TRIPLE_THREE.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldDetect5() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "------xooo------" +
                "------xxxx------" +
                "------xooo------" +
                "------xooo------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertTrue(SituationType.TRIPLE_THREE.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldNotDetect2() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "-------oox------" +
                "-------oox------" +
                "-------oox------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertFalse(SituationType.TRIPLE_THREE.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }
}
