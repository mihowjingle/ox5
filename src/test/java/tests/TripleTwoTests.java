package tests;

import extensions.TestBoard;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import ox5.enums.SituationType;
import ox5.game.elements.Intersection;
import ox5.game.elements.symbols.StandardSymbol;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TripleTwoTests {

    @Test
    void shouldDetect6() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "-------oo-------" +
                "-------oo-------" +
                "------xoo-------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertTrue(SituationType.TRIPLE_TWO.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldDetect1() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "-------oo-------" +
                "-------oo-------" +
                "-------oo-------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertTrue(SituationType.TRIPLE_TWO.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldNotDetect2() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "-------oox------" +
                "-------oox------" +
                "-------oox------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertFalse(SituationType.TRIPLE_TWO.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldDetect2() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "-------oo-x-----" +
                "-------oo-x-----" +
                "-------oo-x-----" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertTrue(SituationType.TRIPLE_TWO.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldNotDetect3() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "-------oo-------" +
                "----------------" +
                "-------oo-------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertFalse(SituationType.TRIPLE_TWO.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    @Disabled
    void shouldNotDetect1() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "-----o----------" +
                "----ooox--------" +
                "--oxxxxo--------" +
                "----oox-x-------" +
                "------ox-x------" +
                "--------x-------" +
                "---------o------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertFalse(SituationType.TRIPLE_TWO.detect(StandardSymbol.CROSS, testBoard).isPresent());
    }

    @Test
    void shouldDetect3() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "-------oo-------" +
                "-------xx-------" +
                "-------oo-------" +
                "-------oo-------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertTrue(SituationType.TRIPLE_TWO.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldDetect8() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "------xoo-------" +
                "-------oo-------" +
                "--------o-------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        final Optional<Intersection> intersection = SituationType.TRIPLE_TWO.detect(StandardSymbol.CIRCLE, testBoard);
        assertTrue(intersection.isPresent());
    }

    @Test
    void shouldDetect4() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "-------oo-------" +
                "-------oo-------" +
                "--------o-------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertTrue(SituationType.TRIPLE_TWO.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldDetect7() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "------xoo-------" +
                "-------oo-------" +
                "------x---------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertTrue(SituationType.TRIPLE_TWO.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }

    @Test
    void shouldDetect5() {
        TestBoard testBoard = new TestBoard(
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "-------oo-------" +
                "-------oo-------" +
                "------x---------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------" +
                "----------------"
        );
        assertTrue(SituationType.TRIPLE_TWO.detect(StandardSymbol.CIRCLE, testBoard).isPresent());
    }
}
